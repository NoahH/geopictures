var page = 1, maxPage = 1, text = "book";
if ("geolocation" in navigator) {
    navigator.geolocation.getCurrentPosition(function(position) {
        getPicture(position.coords.latitude, position.coords.longitude);
    });
}else {
    getPicture(48.8584, 2.2945);
}
function getPicture(lat, lon){
    fetch("https://cors-anywhere.herokuapp.com/https://flickr.com/services/rest/?api_key=5a1a8ef65169a066ded54780b6879e44&format=json&nojsoncallback=1&method=flickr.photos.search&safe_search=1&per_page=1&lat=" + lat + "&lon=" + lon + "&text=" + text + "&page=" + page)
        .then(res => res.json())
        .then(data => {changeImage(data)});
}
function constructImageURL (photoObj) {
    return "https://farm" + photoObj.farm +
            ".staticflickr.com/" + photoObj.server +
            "/" + photoObj.id + "_" + photoObj.secret + ".jpg";
}
function changeImage(data){
    let image = document.createElement("img");
    image.src = constructImageURL(data.photos.photo[0]);
    document.getElementById("imageHolder").innerHTML = "";
    document.getElementById("imageHolder").appendChild(image);
    maxPage = data.photos.pages;
}
document.getElementById("next").addEventListener("click", () => {
    if(page >= maxPage)
        page = 0;
    page ++;
    if ("geolocation" in navigator) {
        navigator.geolocation.getCurrentPosition(function(position) {
            getPicture(position.coords.latitude, position.coords.longitude);
        });
    }else {
        getPicture(48.8584, 2.2945);
    }
}); 
document.getElementById("previous").addEventListener("click", () => {
    if(page > 1)
        page --;
    if ("geolocation" in navigator) {
        navigator.geolocation.getCurrentPosition(function(position) {
            getPicture(position.coords.latitude, position.coords.longitude);
        });
    }else {
        getPicture(48.8584, 2.2945);
    }
}); 
document.getElementById("keyword").addEventListener("keydown", (e) => {
    if(e.keyCode === 13){
        page = 1;
        text = document.getElementById("keyword").value;
        console.log(text);
        if ("geolocation" in navigator) {
            navigator.geolocation.getCurrentPosition(function(position) {
                getPicture(position.coords.latitude, position.coords.longitude);
            });
        }else {
            getPicture(48.8584, 2.2945);
        }
    }
}); 